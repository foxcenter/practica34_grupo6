
CREATE TABLE USUARIO (
no_cuenta SERIAL PRIMARY KEY,
nombre VARCHAR(50),
apellido VARCHAR(50),
dpi VARCHAR(50),
saldo DECIMAL,
correo VARCHAR(50),    
contrasena VARCHAR(50)
);

CREATE TABLE TRANSFERENCIA(
    id_transferencia SERIAL PRIMARY KEY,
    no_cuenta1 INT,
    no_cuenta2 INT,
    fecha DATE,
    monto DECIMAL
);

ALTER TABLE TRANSFERENCIA 
ADD CONSTRAINT  fk_id_cuenta1 foreign key(no_cuenta1) REFERENCES USUARIO(no_cuenta);

ALTER TABLE TRANSFERENCIA 
ADD CONSTRAINT fk_id_cuenta2 foreign key(no_cuenta2) REFERENCES USUARIO(no_cuenta);

alter table USUARIO
  add constraint UQ_usuario
  unique (dpi);
  
-- FUNCTION TRANSFERENCIA 
create function transferir(
	envia int,
	recibe int,
	monto dec
) returns varchar(50)
language plpgsql
as $$
declare 

saldoUser1 dec;
	begin 
		-- get the saldo user 1 
		select saldo into saldoUser1  from usuario where no_cuenta = envia;
		
	if (saldoUser1 >= monto)  then 
		update usuario  set saldo = saldo - monto where no_cuenta  = envia;
	
		update usuario 
		set saldo = saldo + monto 
		where no_cuenta  = recibe;
	
		insert into transferencia(
		no_cuenta1,
		no_cuenta2,
		fecha,
		monto)
		values (envia, recibe , CURRENT_DATE, monto );
		
	else 
		return 'Saldo insuficiente' ; 
	
	end if; 
	return 'Transaccion exitosa' ;	
end;
$$
