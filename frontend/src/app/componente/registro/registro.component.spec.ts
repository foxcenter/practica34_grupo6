import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroComponent } from './registro.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('RegistroComponent', () => {
  let component: RegistroComponent;
  let fixture: ComponentFixture<RegistroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistroComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Registro', () => {
  let component: RegistroComponent;
  let fixture: ComponentFixture<RegistroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistroComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Prueba Unitaria Registro - Success', () => {
    component.usuario = {
      nombre: 'Ricardo ',
      apellido: 'Fuentea',
      dpi: '124',
      saldo: '2',
      correo: 'fuentearicardo@gmail.com',
      contrasena: '2',
    };
    expect(component.Registrarse()).toBeTrue();
  });

  it('Prueba Unitaria Registro - fail', () => {
    component.usuario = {
      nombre: '',
      apellido: '',
      dpi: '124',
      saldo: '2',
      correo: 'fuentearicardo@gmail.com',
      contrasena: '2',
    };
    expect(component.Registrarse()).toBeFalse();
  });
});
