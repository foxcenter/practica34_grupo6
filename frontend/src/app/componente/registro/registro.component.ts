import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ServihttpService } from 'src/app/servicio/servihttp.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  usuario = {
    nombre: '',
    apellido: '',
    dpi: '',
    saldo: '',
    correo: '',
    contrasena: '',
  };
  constructor(private http: ServihttpService) {}

  ngOnInit(): void {}

  Registrarse(): boolean {
    if (
      this.usuario.nombre != '' &&
      this.usuario.nombre.trim().length > 0 &&
      this.usuario.apellido != '' &&
      this.usuario.apellido.trim().length > 0 &&
      this.usuario.dpi != '' &&
      this.usuario.saldo != '' &&
      this.usuario.saldo.trim().length > 0 &&
      this.usuario.correo != '' &&
      this.usuario.correo.trim().length > 0 &&
      this.usuario.contrasena != ''
    ) {
      this.http.registro(this.usuario).subscribe(
        (res) => {
          this.usuario.nombre="";
          this.usuario.apellido="";
          this.usuario.dpi="";
          this.usuario.saldo="";
          this.usuario.contrasena="";
          this.usuario.contrasena="";
          Swal.fire('Usuario creado correctamente.');
        },
        (err) => this.alerta('El DPI que intenta ingresar ya está registrado.')
      );
      return true;
    } else {
      this.alerta('Todos los campos son obligatorios.');
      return false;
    }
  }

  alerta(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }
}
