import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { ServihttpService } from 'src/app/servicio/servihttp.service';
import Swal from 'sweetalert2';
import { jsPDF } from 'jspdf';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css'],
})
export class PrincipalComponent implements OnInit {
  nocuenta: string = '';
  nombre: string = '';
  correo: string = '';
  dpi: string = '';
  datos: any;
  constructor(private http: ServihttpService) {}

  ngOnInit(): void {
    var token = localStorage.getItem('token');
    if (token != null) {
      var bytes = CryptoJS.AES.decrypt(token, 'analisis1');
      var jsontoken = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      this.nocuenta = jsontoken[0].no_cuenta;
      this.nombre = jsontoken[0].nombre + ' ' + jsontoken[0].apellido;
      this.dpi = jsontoken[0].dpi;
      this.transferencias();
    }
  }

  saldo(): boolean {
    if (this.nocuenta != '') {
      this.http.saldo({ no_cuenta: this.nocuenta }).subscribe(
        (res) => {
          Swal.fire('Saldo disponible : Q' + res.saldo);
        },
        (err) => console.log(err)
      );
      return true;
    }
    return false;
  }

  transferencias() {
    if (this.nocuenta != '') {
      this.http.transferencias({ cuenta: this.nocuenta }).subscribe(
        (res) => {
          this.datos = res;
          console.log(this.datos);
        },
        (err) => console.log(err)
      );
      return true;
    }
    return false;
  }

  convertJsonToPdf() {
    var x=10
    console.log(this.datos);
    var doc = new jsPDF();
    
    doc.setFontSize(10);
    for(let item of this.datos){
    doc.text(JSON.stringify(item),1,x);
    x=x+10;
    }
    doc.save('table.pdf');
  }
}
