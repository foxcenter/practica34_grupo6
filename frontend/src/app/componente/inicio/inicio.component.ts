import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ServihttpService } from 'src/app/servicio/servihttp.service';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements OnInit {
  usuario = {
    dpi: '',
    password: '',
  };
  constructor(private http: ServihttpService, private router: Router) {}

  ngOnInit(): void {}

  iniciar():boolean {
    if (
      this.usuario.dpi != '' &&
      this.usuario.dpi.trim().length > 0 &&
      this.usuario.password != ''
    ) {
      this.http.iniciar(this.usuario).subscribe(
        (res) => {
          var ciphertext = CryptoJS.AES.encrypt(
            JSON.stringify(res),
            'analisis1'
          ).toString();
          localStorage.setItem('token', ciphertext);
          this.router.navigate(['/principal']);
          
        },
        (err) => this.alerta('El DPI o contraseña son incorrectos.')
      );
      return true;
    } else {
      this.alerta('Todos los campos son obligatorios.');
      return false;
    }
  }

  alerta(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });



    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta1(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta3(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta4(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta5(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta6(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }


  alerta8(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta89(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }


  alerta45(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }

  alerta787(TipoError: string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    

    Toast.fire({
      icon: 'error',
      title: TipoError,
    });
  }
}
