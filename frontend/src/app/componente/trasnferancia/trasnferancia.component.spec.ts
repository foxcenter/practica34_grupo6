import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasnferanciaComponent } from './trasnferancia.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
describe('TrasnferanciaComponent', () => {
  let component: TrasnferanciaComponent;
  let fixture: ComponentFixture<TrasnferanciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrasnferanciaComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasnferanciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Transacciones', () => {
  let component: TrasnferanciaComponent;
  let fixture: ComponentFixture<TrasnferanciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrasnferanciaComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasnferanciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Prueba Unitaria Transacccion - Success', () => {
    component.usuario = {
      origen: '123',
      destino: '123',
      monto: '100',
    };
    expect(component.transeferir()).toBeTrue();
  });

  it('Prueba Unitaria Transacccion - fail', () => {
    component.usuario = {
      origen: '123',
      destino: '',
      monto: '',
    };
    expect(component.transeferir()).toBeFalse();
  });
});
