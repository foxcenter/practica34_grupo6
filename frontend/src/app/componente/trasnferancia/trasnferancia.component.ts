import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { ServihttpService } from 'src/app/servicio/servihttp.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-trasnferancia',
  templateUrl: './trasnferancia.component.html',
  styleUrls: ['./trasnferancia.component.css'],
})
export class TrasnferanciaComponent implements OnInit {
  usuario = {
    origen: '',
    destino: '',
    monto: '',
  };

  constructor(private http: ServihttpService) {}

  ngOnInit(): void {
    var token = localStorage.getItem('token');
    if (token != null) {
      var bytes = CryptoJS.AES.decrypt(token, 'analisis1');
      var jsontoken = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      this.usuario.origen = jsontoken[0].no_cuenta;
    }
  }

  transeferir() {
    if (
      this.usuario.destino != null &&
      this.usuario.destino.trim().length > 0 &&
      this.usuario.monto != '' &&
      this.usuario.monto.trim().length > 0
    ) {
      this.http.transferir(this.usuario).subscribe(
        (res) => {
          if (res.transferir == 'Saldo insuficiente') {
            this.alerta(
              'El saldo es insuficiente para efectuar la transacción.',
              1
            );
          } else {
            this.usuario.destino = '';
            this.usuario.monto = '';
            this.alerta('La transacción se realizó con éxito.', 2);
          }
        },
        (err) => this.alerta('El numero de cuenta no existe.', 1)
      );
      return true;
    } else {
      this.alerta('Todos los campos son obligatorios.', 1);
      return false;
    }
  }
  
  alerta(TipoError: string, tipo: number): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    if (tipo == 1) {
      Toast.fire({
        icon: 'error',
        title: TipoError,
      });
    } else {
      Toast.fire({
        icon: 'success',
        title: TipoError,
      });
    }
  }
}
