import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './componente/inicio/inicio.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { PrincipalComponent } from './componente/principal/principal.component';
import { TrasnferanciaComponent } from './componente/trasnferancia/trasnferancia.component';
import { ProteccionGuard } from './proteccion.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full',
  },
  {
    path: 'inicio',
    component: InicioComponent,
  },
  {
    path: 'registro',
    component: RegistroComponent,
  },
  {
    path: 'principal',
    component: PrincipalComponent,
    canActivate: [ProteccionGuard]
  },
  {
    path: 'transferencia',
    component: TrasnferanciaComponent,
    canActivate: [ProteccionGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
