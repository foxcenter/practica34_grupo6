import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { ServihttpService } from './servicio/servihttp.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ProteccionGuard implements CanActivate {
  constructor(private http: ServihttpService, private router: Router) {}
  canActivate(): boolean {
    if (this.http.islog()) {
      return true;
    }
    this.router.navigate(['/inicio']);
    return false;
  }
}
