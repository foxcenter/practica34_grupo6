import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class ServihttpService {
  
  private URL = 'http://35.239.79.125:4000/';
  constructor(private http: HttpClient, private router: Router) {}
  registro(user: any) {
    return this.http.post<any>(this.URL + 'registro', user);
  }
  iniciar(user: any) {
    return this.http.post<any>(this.URL + 'ingresar', user);
  }

  saldo(user: any) {
    return this.http.post<any>(this.URL + 'saldo', user);
  }
  transferir(user: any) {
    return this.http.post<any>(this.URL + 'transferir', user);
  }
  transferencias(user: any) {
    return this.http.post<any>(this.URL + 'transferencias', user);
  }

  islog() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
  salir() {
    localStorage.removeItem('token');
    this.router.navigate(['/inicio']);
  }
}
