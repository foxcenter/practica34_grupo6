import { TestBed } from '@angular/core/testing';

import { ServihttpService } from './servihttp.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
describe('ServihttpService', () => {
  let service: ServihttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule]
    });
    service = TestBed.inject(ServihttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
