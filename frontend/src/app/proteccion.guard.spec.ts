import { TestBed } from '@angular/core/testing';

import { ProteccionGuard } from './proteccion.guard';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
describe('ProteccionGuard', () => {
  let guard: ProteccionGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule],
    });
    guard = TestBed.inject(ProteccionGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
