import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './componente/inicio/inicio.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { FormsModule } from '@angular/forms';
import { PrincipalComponent } from './componente/principal/principal.component';
import { TrasnferanciaComponent } from './componente/trasnferancia/trasnferancia.component';
import { HttpClientModule } from '@angular/common/http';
import { ProteccionGuard } from './proteccion.guard';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    RegistroComponent,
    PrincipalComponent,
    TrasnferanciaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ProteccionGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
