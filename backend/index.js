var express = require('express');
const cors = require('cors');
const app = express();
var corsOption = {origin : true , optionSuccessStatus: 200};

const { Client } = require('pg');
var connectionString = "http://postgres:123*abcd@postgresqls:5432/analisisp2";
const client = new Client( { connectionString: connectionString} );
client.connect();

app.use(express.json());
app.use(cors(corsOption));
app.set("port", process.env.PORT || 4000);

app.get("/", async (req,res)=> {
    res.send("Welcome to our endpoint root");
});


app.post('/registro', async (req, res) => {
    const { nombre , apellido, dpi,saldo, correo, contrasena } = req.body;
    let consulta = `insert into USUARIO(nombre, apellido,dpi,saldo,correo,contrasena) 
    VALUES(
    '${nombre}',
    '${apellido}',
    '${dpi}',
    ${saldo},
    '${correo}',
    '${contrasena}'
    )`;
    client.query(consulta, (err, result) => {
        (err) ? res.status(401).send("Todos los campos son necesario") 
            : res.status(200).json({mensaje: `usuario : ${nombre} registrado`});
    })
});


app.post("/ingresar", async (req,res) => {
    const {dpi , password} = req.body;
    let consulta = `select no_cuenta , nombre , apellido , dpi , correo from
    usuario where dpi= '${dpi}' and contrasena= '${password}'    
    `;
    client.query(consulta, (err, result) => {
        if (err) {
            console.log(err);
            res.status(401).send(err);

        }else{
            result.rows[0] != null ? 
                res
                .status(200)
                .send(result.rows):
                res
                .status(401)
                .send("El usuario o contraseña no existen");
        }
    })
});

app.post("/saldo", async (req,res) => {
     const {no_cuenta } = req.body;
     let consult = `select no_cuenta, saldo  from usuario where no_cuenta=${no_cuenta}`;
     client.query(consult, (err, result) => {
         err ? res.status(401).send( 'existe un error ') :
            res.status(200).send(result.rows[0]) 
     })
})

app.post("/transferir", async (req, res) => {
    const {origen , destino , monto} = req.body;
    await client.query(`select transferir(${origen},${destino},${monto})`, (err, result) => {
        if (err) {
            console.log("Error 401 -> ");
            res.status(401).send(err);
        }else{
            res.status(200).send(result.rows[0]);
        }

    })
})

app.post('/transferencias', async (req,res) => {

    const {cuenta} = req.body;
    await client.query(`select * from transferencia where no_cuenta1 = $1`,[cuenta], (err, result) => {
        if (err) {
            console.log("Error 401 -> No se obtuvieron trasferencias");
            res.status(401).send(err);
        }else{
            console.log('Succes 200 -> Obtener transferencias ');
            res.status(200).send(result.rows);
        }
    })
});


app.listen(4000, ()=> {
    console.log("Server is running  on 4000");
});


 module.exports = {app};