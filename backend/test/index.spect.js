const assert = require('assert');
const chaiHttp = require('chai-http');
const { app } = require('../index');
const chai = require('chai') ;
const { expect }  = require('chai');
const  exp = require('constants');
const { captureRejections } = require('events');


chai.use(chaiHttp);



// GET root server 
/**
 *  @autor widjos
 */

describe("Conexion de Prueba Raiz", () => {
    it( 'Debe retornar un mensaje de respuesta', () => {
        return chai.request(app).get('/')
        .then( res => {
            chai.expect(res.text).to.eql('Welcome to our endpoint root');
        })
    })
});

// POST usuario nuevo 
/**
 * @autor widjos
 */

describe('Registro de usuario', ()=> {
    it('Debe retornar un status 200', () => {
        chai.request(app).post('/registro')
        .send({
            nombre : 'Rose',
            apellido: 'Camposeco',
            dpi : '124562321',
            saldo: 50000,
            correo: 'ejemplo@casa.com',
            contrasena : '123456'
        })
        .end ( (err,res) => {
            expect(err).to.be.null;
            //Se espera que el ingreso de datos a la base de datos sea exitoso
            expect(res).to.have.status(200);
        })
    })
})

// POST login  

/**
 * @autor widjos
 */

describe('Login de Usuario', () => {
  it('Debe retornar el id', ()=> {
      chai.request(app).post('/ingresar')
      .send({
          dpi:'54362514',
          password: '123456lu'
      })
      .end ((err, result) => {
          expect(err).to.be.null;
          expect(result).to.have.status(200)
      } )
  })  
})


// POST getsaldo 

/**
 * @autor widjos
 */

 describe('Get saldo usuario', () => {
    it('Retornara  el no_cuetna y su saldo', ()=> {
        chai.request(app).post('/saldo')
        .send({
            no_cuenta: 1
        })
        .then ((err, result) => {
            expect(err).to.be.null;
            expect(result).to.have.status(200)
            it(result.rows);
        } )
    })  
  })

  //POST  Transferencia 

  /**
   * @autor widjos
   */

  describe('Termina la transferencia', () => {
    it('Retorna un mensaje cuando la transaccion ha terminado', () =>{
        chai.request(app).post('/transferir')
        .send({
            origen: 1,
            destino: 2,
            monto : 1000
        })
        .then( (err, result) => {
            expect(err).to.be.null;
            expect(result).to.have.status(200);
               
        })
    })
  });

  //POST get transferencias de un usuario 

/**
 * @author widjos
 */

describe('Obtiene las transferencias', () => {
    it('Tiene que retornar las transferencias', ()=> {
        chai.request(app).post('/transferencias')
        .send(
            {
                cuenta: 1
            }
        )
        .then( (err, result) => {
            expect(err).to.be.null;
            expect(result).to.have.status(200);
               
        })
    })
});